## O instrutor virtual com interface Curses/ambiente texto.  ##

![logo](img/White-new-logo.png)

> O projeto consiste em uma adaptação do instVirt para
> ambiente GUI escrito em python, agora escrito em PERL
> com suporte a interface de texto e frontend ncurses.

## The virtual instructor with Curses/text environment interface. ##
> The project consists of an adaptation of instVirt to
> GUI environment written in python, now written in PERL
> with support for text interface and ncurses frontend.

---

# O desenvolvimento dos estudos #

# The development of studies #


**Suporte**, os estudos estão sendo disponibilizados em três idiomas. O português o inglês e o espanhol.

**Support**, studies are being made available in three languages. Portuguese, English and Spanish.

**Installing**
**Instalando**.

**Easy way/modo fácil.**

With a user on sudo group:

> run the setup script.

./setup.sh

The setup make these steps automaticaly.

Dependências do aplicativo:

Application dependencies:


- General:

- Gerais:

On debian systems:

with super user:

Em sistemas debian:

com super usuário:




~:apt install espeak

With user who is a member of the sudo group:

Com usuário membro do grupo sudo:

~:sudo apt install espeak

- set of voices for **English**:

> support for English language voices is native to the espeak installation, this does not require a set of additional voices.

- conjunto de vozes para o **inglês**:

> o suporte a vozes do idioma inglês é nativo da instalação do espeak, isso faz com que não seja necessário, um conjunto de vozes adicionais.


- installing a set of voices for **Portuguese**:


- instalando conjunto de vozes para o **português**:

>For voice synthesizer support for Portuguese, we have the following voice sets.

>Para o suporte ao sintetizador de voz para o português, temos os seguintes conjuntos de vozes.

~:apt install mbrola-br1 mbrola-br2 mbrola-br3 mbrola-br4


- installing a set of voices for **spanish**:


>Para la compatibilidad con el sintetizador de voz en español, tenemos los siguientes conjuntos de voces.

~:apt install mbrola-es1 mbrola-es2 mbrola-es3 mbrola-es4

Com as dependências instaladas podemos prosseguir para a próxima etapa.

---

**Installing the app**.


**Instalando o aplicativo**.

Cloning the repository:

Clonando o repositório:

git clone https://codeberg.org/Linux77/white-new

>Running the configuration scripts.
> Entering the project directory.

>Executando os scripts de configuração.
> Entrando no diretório do projeto.

~:cd white-new


> Criando os links necessários para o aplicativo localizar os textos associados aos edtudos.

~:./clinks

ou:

bash clinks

> This script will create, and verify the links to the studies.

> Este script criará, e verificará os links para os estudos.


> Criando os scripts usados para as instruções do sintetizador de voz....

~:./cspeaks

ou:

bash cspeaks

> This script will create an auxiliary script for the output with the voice synthesizer.

> Este script criará um script auxiliar para a saída com o sintetizador de vozes.

**testes**.

> Testando o script de configuração do sintetizador de voz....

echo "olá mundo, este é o ambiente de voz em português" | pbastet

> Caso seja reproduzido o audio referente ao texto de exemplo, o ambiente está funcionando corretamente.


**Running the program:**

**Executando o programa:**

> As the project initially supports several languages, it is necessary to inform the desired language as a parameter.
> In its execution we use the syntax:

> Como o projeto inicialmente dá suporte a vários idiomas, é necessário informar como parâmetro, o idioma desejado.
> Em sua execução usamos a sintaxe:

~:./white-new portuguese

> To leave studies in Portuguese.

> Para saída dos estudos em português.

> To leave studies in Spanish.

> Para saída dos estudos em espanhol.

~:./white-new spanish

~:./white-new english

> To leave studies in English.

> Para saída dps estudos em inglês.

---

### The subjects covered ###

The contents programmed for the studies follow the model of a course that I teach, having success in training professionals capable of recycling, having a base in the knowledge necessary for further specialization.

### As disciplinas abordadas ###

O conteúdo programado para os estudos, seguem o modelo de um curso que ministro, tenod sucesso na formação de profissionais capazes de se reciclarem, tendo uma base nos conhecimentos necessários para posteriores especializações.


**Contents**.

The project has 40 studies, ten in each of the following disciplines related to the study of computing.

**Conteúdo**.

O projeto possui 40 estudos, sendo dez em cada uma das seguintes disciplinas relacionadas com o estudo da computação.

**Math**.

>Important topics and basic theory for the study of technology.


**Matemática**.

>Tópicos importantes e teoria de base para o estudo de tecnologia.

**Electricity**.

> Principles and fundamentals of the areas of electricity and electronics.

**Eletricidade**.

> Princípios e fundamentos das áreas de eletricidade e eletrônica.

**Operational systems**.

> Descriptions and definitions of concepts and system operation.

**Sistemas operacionais**.
> Descrições e definições de conceitos e funcionamento de sistemas.

**Information Technology**.

> Knowledge of forms of practical application of technology in human activities.

**Tecnologia da informação**.
> Conhecimento de formas de aplicação prática da tecnologia nas atividades humanas.


[**Website for my courses and studies:**](http://www.asl-sl.com.br)
> Here I provide some studies, projects related to tecnology. I teach in my small city during some time an>

> On my courses and studies **website** I use __portuguese,__ my natural language.


[**Website search engine projects:**](http://magicbyte.tec.br:8888/)


> Here I install one public seaech __engine__ for studies end learning purposes.

> The software used, also promotes one metasearch, and small crawler.

> You are welcome to known and colaborate.

**Estudos, pesquisas, suporte e desenvolvimento.**
> Coordenação: (Prof. __Leonardo__)

> **Linux77.**

**contato:**

### **Mônica.**

**fone:** (35)99853-7574.

**email:** [Mônica](monijucodoro@gmail.com)

[Academia do software livre **Brasil - MG**](http://www.asl-sl.com.br)

[Courses and studies**](http://www.cursos.asl-sl.com.br)

[contact](mailto:feraleomg@gmail.com)
